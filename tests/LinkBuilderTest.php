<?php
// Chargement du fichier d'autochargement de Composer
require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use AngleWeb\HtmlBuilder\LinkBuilder;

class LinkBuilderTest extends TestCase
{
    //
    // Constructor
    //
    public function testConstructorWithNoParameter()
    {
        $tag = new LinkBuilder();
        $this->assertSame(true, empty($tag->getUrl()));
        $this->assertSame('a', $tag->getTag(), 'Tag name is not good');
    }

    public function testConstructorWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new LinkBuilder(new stdClass());
    }

    public function testConstructorWithTwoWrongParameters()
    {
        $this->expectException(TypeError::class);
        $tag = new LinkBuilder(new stdClass(), new stdClass());
    }

    public function testConstructorWithUrl() {
        $url = 'https://angleweb.fr';
        $tag = new LinkBuilder($url);
        $this->assertSame($url, $tag->getUrl(), 'URL is not good');
        $this->assertSame('a', $tag->getTag(), 'Tag name is not good');
    }

    //
    // setTarget
    //

    //
    // getTarget
    //

    //
    // setUrl
    //

    //
    // getUrl
    //

    //
    // checkIfExternalLink
    //

    //
    // checkIfFileLink
    //

    //
    // isNoFollow
    //

    //
    // setNoFollow
    //

    //
    // isExternal
    //

    //
    // setIsExternal
    //

    //
    // isNoOpener
    //

    //
    // setNoOpener
    //

    //
    // isNoReferer
    //

    //
    // setNoReferer
    //

    //
    // isDownload
    //

    //
    // setIsDownload
    //

    //
    // setNoTrackingLink
    //


}
