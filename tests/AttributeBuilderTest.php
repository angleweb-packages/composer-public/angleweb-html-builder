<?php
// Chargement du fichier d'autochargement de Composer
require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use AngleWeb\HtmlBuilder\AttributeBuilder;

class AttributeBuilderTest extends TestCase
{
    //
    // Constructor
    //
    public function testConstructorWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $attr = new AttributeBuilder();
    }

    public function testConstructorWithOneParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $attr = new AttributeBuilder('css');
    }

    public function testConstructorWithOneNoStringParameter()
    {
        $this->expectException(TypeError::class);
        $attr = new AttributeBuilder(new stdClass());
    }

    public function testConstructorWithTwoWrongParameters()
    {
        $this->expectException(TypeError::class);
        $attr = new AttributeBuilder(new stdClass(), new stdClass());
    }

    public function testConstructorWithTwoStringsParameters()
    {
        $attr = new AttributeBuilder('css', 'title');

        $this->assertSame('css', $attr->getName());
        $this->assertSame(true, is_array($attr->getValue()));
        $this->assertSame(1, count($attr->getValue()));
    }

    public function testConstructorWithOneStringsOneArrayParameters()
    {
        $attr = new AttributeBuilder('css', ['title', 'title--strong']);

        $this->assertSame('css', $attr->getName());
        $this->assertSame(true, is_array($attr->getValue()));
        $this->assertSame(2, count($attr->getValue()));
    }

    //
    // setName
    //
    public function testSetNameWithNoString()
    {
        $this->expectException(TypeError::class);
        $attr = new AttributeBuilder('css', 'title');
        $attr->setName(new stdClass());
    }

    public function testSetNameWithString()
    {
        $attr = new AttributeBuilder('css', 'title');
        $attr->setName('id');
        $this->assertSame('id', $attr->getName());
    }

    public function testSetNameWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $attr = new AttributeBuilder();
        $attr->setName();
    }

    //
    // SetValue
    //
    public function testSetValueWithNoStringOrArray()
    {
        $this->expectException(TypeError::class);
        $attr = new AttributeBuilder('css', 'title');
        $attr->setValue(new stdClass());
    }

    public function testSetValueWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $attr = new AttributeBuilder('css', 'title');
        $attr->setValue();
    }

    public function testSetValueWithString()
    {
        $attr = new AttributeBuilder('css', 'title');
        $attr->setValue('list');
        $this->assertSame(true, is_array($attr->getValue()));
        $this->assertSame(1, count($attr->getValue()));
        $this->assertSame(true, array_search('list', $attr->getValue()) !== false);
    }

    public function testSetValueWithArray()
    {
        $attr = new AttributeBuilder('css', 'title');
        $attr->setValue(['list', 'body']);

        $this->assertSame(true, is_array($attr->getValue()));
        $this->assertSame(2, count($attr->getValue()));
        $this->assertSame(true, array_search('list', $attr->getValue()) !== false);
        $this->assertSame(true, array_search('body', $attr->getValue()) !== false);
    }

    //
    // addValue
    //
    public function testAddValueWithObject()
    {
        $this->expectException(TypeError::class);
        $attr = new AttributeBuilder('css', 'title');
        $attr->addValue(new stdClass());
    }

    public function testAddValueWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $attr = new AttributeBuilder('css', 'title');
        $attr->addValue();
    }

    public function testAddValueWithString()
    {
        $attr = new AttributeBuilder('css', 'title');
        $attr->addValue('list');
        $this->assertSame(true, is_array($attr->getValue()));
        $this->assertSame(2, count($attr->getValue()));
        $this->assertSame(true, array_search('list', $attr->getValue()) !== false);
        $this->assertSame(true, array_search('title', $attr->getValue()) !== false);
    }

    public function testAddValueWithArray()
    {
        $attr = new AttributeBuilder('css', 'title');
        $attr->addValue(['list', 'body']);

        $this->assertSame(true, is_array($attr->getValue()));
        $this->assertSame(3, count($attr->getValue()));
        $this->assertSame(true, array_search('title', $attr->getValue()) !== false);
        $this->assertSame(true, array_search('list', $attr->getValue()) !== false);
        $this->assertSame(true, array_search('body', $attr->getValue()) !== false);
    }

    //
    // removeValue
    //
    public function testRemoveValueWithObject()
    {
        $this->expectException(TypeError::class);
        $attr = new AttributeBuilder('css', 'title');
        $attr->removeValue(new stdClass());
    }

    public function testRemoveAllValues()
    {
        $attr = new AttributeBuilder('css', ['title', 'title--strong']);
        $attr->removeValue();

        $this->assertSame(true, is_array($attr->getValue()));
        $this->assertSame(true, empty($attr->getValue()));
    }

    public function testRemoveSelectedValue()
    {
        $attr = new AttributeBuilder('css', ['title', 'title--strong']);
        $attr->removeValue('title');

        $this->assertSame(false, array_search('title', $attr->getValue()));
    }

    public function testRemoveSelectedValueThatNotExist()
    {
        $attr = new AttributeBuilder('css', ['title', 'title--strong']);
        $attr->removeValue('title--light');

        $this->assertSame(2, count($attr->getValue()));
        $this->assertSame(true, array_search('title', $attr->getValue()) !== false);
        $this->assertSame(true, array_search('title--strong', $attr->getValue()) !== false);
    }

    //
    // setSeparator
    //
    public function testSetSeparatorWithObject()
    {
        $this->expectException(TypeError::class);
        $attr = new AttributeBuilder('css', 'title');
        $attr->setSeparator(new stdClass());
    }

    public function testSetSeparatorWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $attr = new AttributeBuilder('css', 'title');
        $attr->setSeparator();
    }

    public function testSetSeparatorWithString()
    {
        $attr = new AttributeBuilder('css', 'title');
        $attr->setSeparator('|');

        $this->assertSame('|', $attr->getSeparator());
    }

    //
    // getSeparator
    //
    public function testGetSeparatorDefaultValue()
    {
        $attr = new AttributeBuilder('css', 'title');

        $this->assertSame(' ', $attr->getSeparator());
    }

    //
    // sanitizeValue
    //
    public function testSanitizeValueWithObject()
    {
        $this->expectException(TypeError::class);
        AttributeBuilder::sanitizeValue(new stdClass());
    }

    public function testSanitizeValueWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        AttributeBuilder::sanitizeValue();
    }

    public function testSanitizeValueWithString()
    {
        $sanitize = AttributeBuilder::sanitizeValue('Hors<TAG>\'Tag');

        $this->assertSame(true, is_string($sanitize));
        $this->assertSame('Hors&#039;Tag', $sanitize);
    }

    public function testSanitizeValueWithArray()
    {
        $sanitize = AttributeBuilder::sanitizeValue(['Hors<TAG>\'Tag', 'normal']);

        $this->assertSame(true, is_array($sanitize));
        $this->assertSame(2, count($sanitize));
        $this->assertSame(true, array_search('Hors&#039;Tag', $sanitize) !== false);
        $this->assertSame(true, array_search('normal', $sanitize) !== false);
    }

    //
    // Build
    //
    public function testBuild()
    {
        $attr = new AttributeBuilder('css', ['title', 'title--strong']);

        $this->assertSame('css="title title--strong"', $attr->build());
    }

    public function testBuildWithDuplicateValues()
    {
        $attr = new AttributeBuilder('css', ['title', 'title--strong', 'title']);

        $this->assertSame('css="title title--strong"', $attr->build(), 'Values are duplicated');
    }

    public function testBuildWithSeparator()
    {
        $attr = new AttributeBuilder('keywords', ['test', 'phpunit', 'test case']);
        $attr->setSeparator(',');

        $this->assertSame('keywords="test,phpunit,test case"', $attr->build());
    }
}
