<?php
// Chargement du fichier d'autochargement de Composer
require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use AngleWeb\HtmlBuilder\TagBuilder;

class TagBuilderTest extends TestCase
{
    //
    // Constructor
    //
    public function testConstructorWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder();
    }

    public function testConstructorWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder(new stdClass());
    }

    public function testConstructorWithTwoWrongParameters()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder(new stdClass(), new stdClass());
    }

    public function testConstructorWithOneString()
    {
        $tag = new TagBuilder('div');

        $this->assertSame('div', $tag->getTag(), 'Tag does not contain good value');
        $this->assertSame(true, is_array($tag->getChildren()), 'Children tag is not an array');
        $this->assertSame(0, count($tag->getChildren()), 'Children tag is not empty');
    }

    public function testConstructorWithTwoStringsParameters()
    {
        $tag = new TagBuilder('div', 'mon contenu');

        $this->assertSame('div', $tag->getTag(), 'Tag does not contain good value');
        $this->assertSame(true, is_array($tag->getChildren()), 'Children tag is not an array');
        $this->assertSame(1, count($tag->getChildren()), 'Children tag must have one value');
    }

    public function testConstructorWithOneStringAndArray()
    {
        $tag1 = new TagBuilder('div1', 'mon contenu 1');
        $tag2 = new TagBuilder('div2', 'mon contenu 2');

        $tag = new TagBuilder('div', [$tag1, $tag2]);

        $this->assertSame('div', $tag->getTag(), 'Tag does not contain good value');
        $this->assertSame(true, is_array($tag->getChildren()), 'Children tag is not an array');
        $this->assertSame(2, count($tag->getChildren()), 'Children tag must have two value');
        $this->assertSame('div1', $tag->getChildren()[0]->getTag(), 'Tag name of first child does not contain a good value');
        $this->assertSame('div2', $tag->getChildren()[1]->getTag(), 'Tag name of second child does not contain a good value');
    }

    public function testConstructorWithOneStringAndTagBuilderObject()
    {
        $tag1 = new TagBuilder('div1', 'mon contenu 1');

        $tag = new TagBuilder('div', $tag1);

        $this->assertSame('div', $tag->getTag(), 'Tag does not contain good value');
        $this->assertSame(true, is_array($tag->getChildren()), 'Children tag is not an array');
        $this->assertSame(1, count($tag->getChildren()), 'Children tag must have one value');
        $this->assertSame('div1', $tag->getChildren()[0]->getTag(), 'Tag name of child does not contain a good value');
    }

    //
    // setTag
    //
    public function testSetTagWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->setTag(new stdClass());
    }

    public function testSetTagWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->setTag();
    }

    public function testSetTagWithString()
    {
        $tag = new TagBuilder('div');
        $tag->setTag('h1');
        $this->assertSame('h1', $tag->getTag(), 'Tag does not contain a good value');
    }

    //
    // get/setAttributes
    //
    public function testSetAttributeWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->setAttribute(new stdClass(), new stdClass());
    }

    public function testSetAttributeWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->setAttribute();
    }

    public function testSetAttributeWithEmptySecondParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->setAttribute('id');
    }

    public function testAttributesWithEmptyAttribute()
    {
        $tag = new TagBuilder('div');
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(true, empty($tag->getAttributes()), 'Attributes array is not empty');
    }

    public function testAttributesWithSingleAttribute()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('css', 'title');
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(1, count($tag->getAttributes()), 'Attributes array must contain one value');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'Attribute in array is not instance of AttributeBuilder');
    }

    public function testAttributesWithMultipleAttributes()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('css', 'title');
        $tag->setAttribute('id', 'main');
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(2, count($tag->getAttributes()), 'Attributes array must contain two values');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have first good key');
        $this->assertSame(true, array_key_exists('id', $tag->getAttributes()), 'Attribute array does not have second good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'First attribute in array is not instance of AttributeBuilder');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['id']), 'Second attribute in array is not instance of AttributeBuilder');
    }

    public function testAttributesWithArrayAttributes()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('css', ['title', 'is-1']);
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(1, count($tag->getAttributes()), 'Attributes array must contain two values');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have first good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'First attribute in array is not instance of AttributeBuilder');
        $this->assertSame(2, count($tag->getAttributes()['css']->getValue()), 'Attribute must have 2 values');
    }

    //
    // TODO: getChildren
    //
    public function testGetChildrenWithEmptyChildren()
    {
        $tag = new TagBuilder('div');
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(true, empty($tag->getChildren()), 'Children array is not empty');
    }

    public function testGetChildrenWithOneTagBuilderChild()
    {
        $tag = new TagBuilder('ul');
        $childTag = new TagBuilder('li');
        $tag->addChild($childTag);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(1, count($tag->getChildren()), 'Children array must contain one value');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[0]), 'Child is not instance of TagBuilder');
    }

    public function testGetChildrenWithOneStringChild()
    {
        $tag = new TagBuilder('div');
        $stringChild = 'String tag content';
        $tag->addChild($stringChild);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(1, count($tag->getChildren()), 'Children array must contain one value');
        $this->assertSame(true, is_string($tag->getChildren()[0]), 'Child value is not a string');
        $this->assertSame($stringChild, $tag->getChildren()[0], 'Child value is not the good value');
    }

    public function testGetChildrenWithStringArrayChildren()
    {
        $tag = new TagBuilder('div');
        $stringFirstChild = 'First string tag content';
        $stringSecondChild = 'Second string tag content';
        $tag->addChild([$stringFirstChild, $stringSecondChild]);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(2, count($tag->getChildren()), 'Children array must contain one value');
        $this->assertSame(true, is_string($tag->getChildren()[0]), 'First child value is not a string');
        $this->assertSame(true, is_string($tag->getChildren()[1]), 'Second child value is not a string');
        $this->assertSame($stringFirstChild, $tag->getChildren()[0], 'First child value is not the good value');
        $this->assertSame($stringSecondChild, $tag->getChildren()[1], 'Second child value is not the good value');
    }

    public function testGetChildrenWithTagBuilderArrayChildren()
    {
        $tag = new TagBuilder('div');
        $firstChild = new TagBuilder('h1');
        $secondChild = new TagBuilder('h2');
        $tag->addChild([$firstChild, $secondChild]);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(2, count($tag->getChildren()), 'Children array must contain one value');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[0]), 'First child is not instance of TagBuilder');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[1]), 'Second child is not instance of TagBuilder');
    }

    public function testGetChildrenWithMixArrayChildren()
    {
        $tag = new TagBuilder('div');
        $stringFirstChild = 'String tag content';
        $tagSecondChild = new TagBuilder('div');
        $tag->addChild([$stringFirstChild, $tagSecondChild]);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(2, count($tag->getChildren()), 'Children array must contain one value');
        $this->assertSame(true, is_string($tag->getChildren()[0]), 'First child value is not a string');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[1]), 'Second child is not instance of TagBuilder');
        $this->assertSame($stringFirstChild, $tag->getChildren()[0], 'first child value is not the good value');
    }

    //
    // TODO: addAttributeValue
    //
    public function testAddAttributeValueWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->addAttributeValue(new stdClass(), new stdClass());
    }

    public function testAddAttributeValueWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->addAttributeValue();
    }

    public function testAddAttributeValueWithEmptySecondParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->addAttributeValue('id');
    }

    public function testAddAttributeValueWithStringParameter()
    {
        $tag = new TagBuilder('div');
        $tag->addAttributeValue('css', 'title');
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(1, count($tag->getAttributes()), 'Attributes array must contain one value');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'Attribute in array is not instance of AttributeBuilder');
    }

    public function testAddAttributeValueSetAndAddWithStringParameter()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('css', 'title');
        $tag->addAttributeValue('css', 'is-1');
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(1, count($tag->getAttributes()), 'Attributes array must contain one value');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'Attribute in array is not instance of AttributeBuilder');
        $this->assertSame(2, count($tag->getAttributes()['css']->getValue()), 'Attribute must have 2 values');
    }

    public function testAddAttributeValueSetAndAddWithArrayParameter()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('css', 'title');
        $tag->addAttributeValue('css', ['is-1', 'title--small']);
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(1, count($tag->getAttributes()), 'Attributes array must contain one value');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'Attribute in array is not instance of AttributeBuilder');
        $this->assertSame(3, count($tag->getAttributes()['css']->getValue()), 'Attribute must have 3 values');
    }

    //
    // TODO: addAttribute
    //
    public function testAddAttributeWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->addAttribute(new stdClass(), new stdClass());
    }

    public function testAddAttributeWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->addAttribute();
    }

    public function testAddAttributeWithEmptySecondParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->addAttribute('id');
    }

    public function testAddAttributeWithStringParameter()
    {
        $tag = new TagBuilder('div');
        $tag->addAttribute('css', 'title');
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(1, count($tag->getAttributes()), 'Attributes array must contain one value');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'Attribute in array is not instance of AttributeBuilder');
    }

    public function testAddAttributeSetAndAddWithStringParameter()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('css', 'title');
        $tag->addAttribute('css', 'is-1');
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(1, count($tag->getAttributes()), 'Attributes array must contain one value');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'Attribute in array is not instance of AttributeBuilder');
        $this->assertSame(2, count($tag->getAttributes()['css']->getValue()), 'Attribute must have 2 values');
    }

    public function testAddAttributeSetAndAddWithArrayParameter()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('css', 'title');
        $tag->addAttribute('css', ['is-1', 'title--small']);
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(1, count($tag->getAttributes()), 'Attributes array must contain one value');
        $this->assertSame(true, array_key_exists('css', $tag->getAttributes()), 'Attribute array does not have good key');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttributes()['css']), 'Attribute in array is not instance of AttributeBuilder');
        $this->assertSame(3, count($tag->getAttributes()['css']->getValue()), 'Attribute must have 3 values');
    }

    //
    // TODO: removeAttribute
    //
    public function testRemoveAttributeWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->removeAttribute(new stdClass());
    }

    public function testRemoveAttributeWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->removeAttribute();
    }

    public function testRemoveAttribute()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('id', 'main');
        $tag->removeAttribute('id');
        $this->assertSame(true, is_array($tag->getAttributes()), 'Attributes array is not an array');
        $this->assertSame(true, empty($tag->getAttributes()), 'Attributes array must be empty');
    }

    //
    // TODO: removeAttributeValue
    //
    public function testRemoveAttributeValueWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->removeAttributeValue(new stdClass(), new stdClass());
    }

    public function testRemoveAttributeValueWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->removeAttributeValue();
    }

    public function testRemoveAttributeValueWithEmptySecondParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->removeAttributeValue('id');
    }

    public function testRemoveAttributeValue()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('css', ['title', 'title--small']);
        $tag->removeAttributeValue('css', 'title--small');
        $cssAttributes = $tag->getAttribute('css');
        $this->assertSame(false, array_search('title--small', $cssAttributes->getValue()));
    }

    //
    // TODO: getAttribute
    //
    public function testGetAttributeWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->getAttribute(new stdClass());
    }

    public function testGetAttributeWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->getAttribute();
    }

    public function testGetAttributeFromNoAttributes()
    {
        $tag = new TagBuilder('div');
        $this->assertSame(true, is_null($tag->getAttribute('id')), 'Attribute is not null');
    }

    public function testGetAttribute()
    {
        $tag = new TagBuilder('div');
        $tag->setAttribute('id', 'main');
        $this->assertSame('AngleWeb\HtmlBuilder\AttributeBuilder', get_class($tag->getAttribute('id')), 'Attribute is not instance of AttributeBuilder');
    }

    //
    // TODO: addChild
    //
    public function testAddChildWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->addChild(new stdClass(), new stdClass());
    }

    public function testAddChildWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->addChild();
    }

    public function testAddChildWithTagBuilder()
    {
        $tag = new TagBuilder('div');
        $childTag = new TagBuilder('strong');
        $tag->addChild($childTag);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(1, count($tag->getChildren()), 'Children Array does not have one value');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[0]), 'Child is not instance of TagBuilder');
    }

    public function testAddChildWithString()
    {
        $tag = new TagBuilder('div');
        $childString = 'div content string';
        $tag->addChild($childString);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(1, count($tag->getChildren()), 'Children Array does not have one value');
        $this->assertSame(true, is_string($tag->getChildren()[0]), 'Child is not a string');
    }

    public function testAddChildWithArrayAndEmptyChildren()
    {
        $tag = new TagBuilder('div');
        $childTag = new TagBuilder('strong');
        $childString = 'div content string';
        $tag->addChild([$childTag, $childString]);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(2, count($tag->getChildren()), 'Children Array does not have one value');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[0]), 'First child is not instance of TagBuilder');
        $this->assertSame(true, is_string($tag->getChildren()[1]), 'Second child is not a string');
    }

    public function testAddChildWithArray()
    {
        $tag = new TagBuilder('div');
        $firstTag = new TagBuilder('h1');
        $tag->setChildren([$firstTag]);
        $childTag = new TagBuilder('strong');
        $childString = 'div content string';
        $tag->addChild([$childTag, $childString]);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(3, count($tag->getChildren()), 'Children Array does not have one value');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[0]), 'First child is not instance of TagBuilder');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[1]), 'Second child is not instance of TagBuilder');
        $this->assertSame(true, is_string($tag->getChildren()[2]), 'Third child is not a string');
    }

    //
    // TODO: setChildren
    //
    public function testSetChildrenWithObject()
    {
        $this->expectException(TypeError::class);
        $tag = new TagBuilder('div');
        $tag->setChildren(new stdClass());
    }

    public function testSetChildrenWithNoParameter()
    {
        $this->expectException(ArgumentCountError::class);
        $tag = new TagBuilder('div');
        $tag->setChildren();
    }

    public function testSetChildrenWithArray()
    {
        $tag = new TagBuilder('div');
        $childTag = new TagBuilder('strong');
        $childString = 'div content string';
        $tag->setChildren([$childTag, $childString]);
        $this->assertSame(true, is_array($tag->getChildren()), 'Children array is not an array');
        $this->assertSame(2, count($tag->getChildren()), 'Children Array does not have one value');
        $this->assertSame('AngleWeb\HtmlBuilder\TagBuilder', get_class($tag->getChildren()[0]), 'First child is not instance of TagBuilder');
        $this->assertSame(true, is_string($tag->getChildren()[1]), 'Second child is not a string');
    }

    //
    // TODO: build
    //
    public function testBuild()
    {
        $body = new TagBuilder('body');
        $section1 = new TagBuilder('section', 'Section 1');
        $section2 = new TagBuilder('section', 'Section 2');
        $body->setChildren([$section1, $section2]);
        $body->setAttribute('css', 'body--main');
        $body->setAttribute('id', 'main');

        $this->assertSame('<body css="body--main" id="main"><section>Section 1</section><section>Section 2</section></body>', $body->build());
    }
}
