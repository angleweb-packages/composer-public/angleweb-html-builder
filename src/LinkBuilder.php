<?php

namespace AngleWeb\HtmlBuilder;

/**
 * Create ahref link tag
 */
class LinkBuilder extends TagBuilder
{
    public const REL_NO_FOLLOW = 'nofollow';
    public const REL_NO_OPENER = 'noopener';
    public const REL_NO_REFERER = 'noreferer';
    public const REL_EXTERNAL = 'external';

    private string $url;
    private string $target;
    private bool $noFollow = false;
    private bool $noOpener = false;
    private bool $noReferer = false;
    private bool $isExternal = false;
    private bool $isDownload = false;

    protected array $targetsEnabled = [
        '_blank',
        '_parent',
        '_self',
        '_top',
    ];

    /**
     * Constructor
     *
     * @param string|null $url
     * @param array|string|TagBuilder|null $content
     */
    public function __construct(string $url = null, array|string|TagBuilder $content = null)
    {
        parent::__construct('a', $content);

        if (isset($url)) {
            $this->setUrl($url);
        }
    }

    /**
     * Set link target
     *
     * @param string $target blank | parent | self | top. If empy then remove target value.
     *
     * @return $this
     */
    public function setTarget(string $target): self
    {
        // If not begins by _ then add it
        if (\strpos('_', $target) !== 0) {
            $target = '_' . $target;
        }

        // Check if target enabled
        if (in_array($target, $this->targetsEnabled)) {
            $this->target = $target;
            $this->addAttribute('target', $target);
        } elseif (empty($target)) {
            $this->removeAttribute('target');
        }

        return $this;
    }

    /**
     * Return ahref target value
     *
     * @return string|null
     */
    public function getTarget(): string|null
    {
        if (isset($this->target)) {
            return $this->target;
        } else {
            return null;
        }
    }

    /**
     * Set URL href link
     *
     * @param string $url absolute or relative URL
     * @param boolean $tracking if true then define link to notracking instruction. Default: false.
     * @param string|null $httpRoot necessary to automatic external link detection
     *
     * @return self
     */
    public function setUrl(string $url, bool $tracking = false, string $httpRoot = null): self
    {
        $this->url = $url;
        $this->setAttribute('href', $url);

        if ($tracking) {
            $this->setNoTrackingLink();
        }

        if (isset($httpRoot)) {
            if (self::checkIfExternalLink($url, $httpRoot)) {
                $this->setIsExternal(true);
            } else {
                $this->setIsExternal(false);
            }
        }

        return $this;
    }

    /**
     * Get Ahref URL
     *
     * @return string|null
     */
    public function getUrl(): string|null
    {
        if (isset($this->url)) {
            return $this->url;
        } else {
            return null;
        }
    }

    /**
     * Check if the link is an external link
     *
     * @param string $url URL to check
     * @param string $httpRoot Absolute URL reference
     *
     * @return boolean
     */
    public static function checkIfExternalLink(string $url, string $httpRoot): bool
    {
        $urlDetails = parse_url($url);

        // If no domain then relative
        // Or if start with httpRoot
        return \array_key_exists('host', $urlDetails)
            && strpos($url, $httpRoot) !== 0;
    }

    /**
     * Check if the link is a file to download
     *
     * @param string $url URL to check
     *
     * @return boolean
     */
    public static function checkIfFileLink(string $url): bool
    {
        $path = parse_url($url, PHP_URL_PATH);
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        $excludeUrl = [
            'htm',
            'html',
            'php'
        ];

        if (!empty($ext) && \array_search($ext, $excludeUrl) === false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the value of noFollow
     */
    public function isNoFollow(): bool
    {
        return $this->noFollow;
    }

    /**
     * Set the value of noFollow
     * add or remove nofollow rel attribute
     */
    public function setNoFollow(bool $noFollow = true): self
    {
        $this->noFollow = $noFollow;
        if ($noFollow) {
            $this->addAttribute('rel', self::REL_NO_FOLLOW);
        } else {
            $this->removeAttributeValue('rel', self::REL_NO_FOLLOW);
        }

        return $this;
    }

    /**
     * Get the value of isExternal
     */
    public function isExternal(): bool
    {
        return $this->isExternal;
    }

    /**
     * Set the value of isExternal
     * add or remove external rel attribute
     */
    public function setIsExternal(bool $isExternal = true): self
    {
        $this->isExternal = $isExternal;
        if ($isExternal) {
            $this->addAttribute('rel', self::REL_EXTERNAL);
        } else {
            $this->removeAttributeValue('rel', self::REL_EXTERNAL);
        }

        return $this;
    }

    /**
     * Get the value of noOpener
     */
    public function isNoOpener(): bool
    {
        return $this->noOpener;
    }

    /**
     * Set the value of noOpener
     * add or remove noopener rel attribute
     */
    public function setNoOpener(bool $noOpener = true): self
    {
        $this->noOpener = $noOpener;
        if ($noOpener) {
            $this->addAttribute('rel', self::REL_NO_OPENER);
        } else {
            $this->removeAttributeValue('rel', self::REL_NO_OPENER);
        }

        return $this;
    }

    /**
     * Get the value of noReferer
     */
    public function isNoReferer(): bool
    {
        return $this->noReferer;
    }

    /**
     * Set the value of noReferer
     * add or remove noreferer rel attribute
     */
    public function setNoReferer(bool $noReferer = true): self
    {
        $this->noReferer = $noReferer;
        if ($noReferer) {
            $this->addAttribute('rel', self::REL_NO_REFERER);
        } else {
            $this->removeAttributeValue('rel', self::REL_NO_REFERER);
        }

        return $this;
    }

    /**
     * Get the value of isDownload
     */
    public function isDownload(): bool
    {
        return $this->isDownload;
    }

    /**
     * Set the value of isDownload
     * Add download attribute
     */
    public function setIsDownload(bool $isDownload = true): self
    {
        $this->isDownload = $isDownload;
        if ($isDownload) {
            $this->addAttribute('download', '');
        } else {
            $this->removeAttribute('download');
        }

        return $this;
    }

    /**
     * Set All tracking relative rel attribute
     * - nofollow
     * - noopener
     * - noreferer
     *
     * @return self
     */
    public function setNoTrackingLink(): self
    {
        $this->setNoFollow();
        $this->setNoOpener();
        $this->setNoReferer();

        return $this;
    }
}
