<?php

namespace AngleWeb\HtmlBuilder;

/**
 * HTML Tag Builder
 *
 * A class for building HTML elements using the Builder pattern.
 */
class TagBuilder
{
    /**
     * The tag name of the HTML element.
     *
     * @var string
     */
    private $tag;

    /**
     * The list of child elements for the HTML element.
     *
     * @var string|TagBuilder[]
     */
    private $children = [];

    /**
     * The list of attributes for the HTML element.
     *
     * @var array AttributeBuilder array
     */
    private $attributes = [];

    /**
     * Constructor
     *
     * @param string $tag
     * @param array|string|TagBuilder|null $content
     */
    public function __construct(string $tag, array|string|TagBuilder $content = null)
    {
        $this->tag = $tag;

        if (!\is_null($content)) {
            $this->addChild($content);
        }
    }

    /**
     * Retourne le nom du tag HTML.
     * @return string Le nom du tag HTML
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * Modifie le nom du tag HTML.
     * @param string $tag Le nouveau nom du tag HTML
     */
    public function setTag(string $tag): void
    {
        $this->tag = $tag;
    }

    /**
     * Get the attributes array.
     *
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * Get the children array.
     *
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * Set an attribute.
     *
     * @param string $name
     * @param string|array $value
     * @return $this
     */
    public function setAttribute(string $name, string|array $value): self
    {
        $this->attributes[$name] = new AttributeBuilder($name, $value);
        return $this;
    }

    /**
     * Add value(s) to the named attribute
     *
     * @param string $name
     * @param string|array $value
     *
     * @return self
     */
    public function addAttributeValue(string $name, string|array $value): self
    {
        if (\array_key_exists($name, $this->attributes)) {
            $this->attributes[$name]->addValue($value);
        } else {
            $this->setAttribute($name, $value);
        }

        return $this;
    }

    /**
     * addAttributeValue alias. Add value(s) to the named attribute
     *
     * @param string $name
     * @param string|array $value
     *
     * @return self
     */
    public function addAttribute(string $name, string|array $value): self
    {
        return $this->addAttributeValue($name, $value);
    }

    /**
     * Remove a named attribute
     *
     * @param string $name Name of the Attribute
     *
     * @return self
     */
    public function removeAttribute(string $name): self
    {
        if ($this->getAttribute($name)) {
            unset($this->attributes[$name]);
        }

        return $this;
    }

    /**
     * Remove a value from named attribute
     *
     * @param string $name Name of the Attribbute
     * @param string $value Remove this value from the attribute
     *
     * @return self
     */
    public function removeAttributeValue(string $name, string $value): self
    {
        if ($attribute = $this->getAttribute($name)) {
            $attribute->removeValue($value);
        }

        return $this;
    }

    /**
     * Get an attribute by name.
     *
     * @param string $name
     * @return AttributeBuilder|null
     */
    public function getAttribute(string $name): ?AttributeBuilder
    {
        return $this->attributes[$name] ?? null;
    }

    /**
     * Add a child element.
     *
     * @param TagBuilder|string|array $child
     * @return $this
     */
    public function addChild(TagBuilder|string|array $child): self
    {
        if (is_array($child)) {
            foreach ($child as $subnode) {
                $this->addChild($subnode);
            }
        } else {
            $this->children[] = $child;
        }

        return $this;
    }

    /**
     * Set the children array.
     *
     * @param array $children
     * @return $this
     */
    public function setChildren(array $children): self
    {
        $this->children = $children;
        return $this;
    }

    /**
     * Renders the HTML element as a string.
     *
     * @return string
     */
    public function build(): string
    {
        // Render attributes
        $attributesHtml = '';

        // Array of attributes rendered
        $attributes = [];

        /** @var AttributeBuilder $attr */
        foreach ($this->attributes as $attr) {
            $attributes[] = $attr->build();
        }

        if (count($attributes) > 0) {
            $attributesHtml = ' ' . implode(' ', $attributes);
        }

        // Render Children
        $childrenHtml = '';
        foreach ($this->children as $child) {
            if ($child instanceof TagBuilder) {
                $childrenHtml .= $child->build();
            } else {
                $childrenHtml .= htmlspecialchars($child, ENT_QUOTES);
            }
        }

        return sprintf('<%1$s%2$s>%3$s</%1$s>', $this->tag, $attributesHtml, $childrenHtml);
    }
}
