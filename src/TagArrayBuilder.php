<?php

namespace AngleWeb\HtmlBuilder;

/**
 * Create ahref link tag
 */
class TagArray extends TagBuilder
{
    public function __construct()
    {
        $this->setTag('');
    }

    public function build(): string
    {
        $builder = '';

        foreach ($this->getChildren() as $child) {
            $builder .= $child->build();
        }

        return $builder;
    }
}
