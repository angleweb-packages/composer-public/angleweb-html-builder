<?php

namespace AngleWeb\HtmlBuilder;

/**
 * Create ahref link tag
 */
class ImgBuilder extends TagBuilder
{
    private string $src;
    private string $alt = '';
    private string $legend;
    private int $width;
    private int $height;
    private bool $isLazy = false;
    private bool $isFigure = false;

    public function __construct()
    {
        $this->setTag('img');
    }

    /**
     * Get the value of src
     */
    public function getSrc(): string
    {
        return $this->src;
    }

    /**
     * Set the value of src
     */
    public function setSrc(string $src): self
    {
        $this->src = $src;
        $this->setAttribute('src', $src);

        return $this;
    }

    /**
     * Get the value of alt
     */
    public function getAlt(): string
    {
        return $this->alt;
    }

    /**
     * Set the value of alt
     */
    public function setAlt(string $alt): self
    {
        $this->alt = $alt;
        $this->setAttribute('alt', $alt);

        return $this;
    }

    /**
     * Get the value of width
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * Set the value of width
     */
    public function setWidth(int $width): self
    {
        if ($width > 0) {
            $this->width = $width;
            $this->setAttribute('width', $width);
        }

        return $this;
    }

    /**
     * Get the value of height
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * Set the value of height
     */
    public function setHeight(int $height): self
    {
        if ($height > 0) {
            $this->height = $height;
            $this->setAttribute('height', $height);
        }

        return $this;
    }

    public function setSize(int $width, int $height): self
    {
        $this->setWidth($width);
        $this->setHeight($height);

        return $this;
    }

    /**
     * Get the value of isLazy
     */
    public function isLazy(): bool
    {
        return $this->isLazy;
    }

    /**
     * Set the value of isLazy
     */
    public function setIsLazy(bool $isLazy = 0): self
    {
        $this->isLazy = $isLazy;

        if ($isLazy === true) {
            $this->addAttribute('loading', 'lazy');
        }

        return $this;
    }

    /**
     * Get the value of legend
     */
    public function getLegend(): string
    {
        return $this->legend;
    }

    /**
     * Set the value of legend
     */
    public function setLegend(string $legend): self
    {
        $this->legend = $legend;

        return $this;
    }

    /**
     * Get the value of isFigure
     */
    public function isFigure(): bool
    {
        return $this->isFigure;
    }

    /**
     * Set the value of isFigure
     */
    public function setIsFigure(bool $isFigure): self
    {
        $this->isFigure = $isFigure;

        return $this;
    }

    public function build(): string
    {
        if ($this->isFigure() || !empty($this->getLegend())) {
            $figure = new TagBuilder('figure', $this);

            if (!empty($this->getLegend())) {
                $legend = new TagBuilder('figcaption', $this->getLegend());
                $figure->addChild($legend);
            }

            return $figure->build();
        } else {
            return parent::build();
        }
    }
}
