<?php

namespace AngleWeb\HtmlBuilder;

/**
 * HTML Attribute Builder
 *
 * A class for building HTML attributes as key-value pairs.
 */
class AttributeBuilder
{
    /**
     * Attribute name
     *
     * @var string
     */
    private string $name;

    /**
     * Attribute value(s)
     *
     * @var array
     */
    private array $value;

    /**
     * Separator to concatenate several values
     *
     * @var string Space as default value
     */
    private string $separator = ' ';

    /**
     * Constructor.
     *
     * @param string $name  Attribute name.
     * @param array|string $value  Attribute value(s).
     */
    public function __construct(string $name, array|string $value)
    {
        $this->setName($name);
        $this->setValue($value);
    }

    /**
     * Set the attribute name.
     *
     * @param string $name  Attribute name.
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the attribute name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name ?? null;
    }

    /**
     * Set the attribute value(s).
     *
     * @param string|array $value  Attribute value(s).
     * @return self
     */
    public function setValue(string|array $value): self
    {
        // Empty array
        $this->value = [];
        $this->addValue($value);

        return $this;
    }

    /**
     * Get the attribute value(s).
     *
     * @return string|array|null
     */
    public function getValue()
    {
        return $this->value ?? null;
    }

    /**
     * Add a value to the attribute.
     *
     * @param string|array $value  Attribute value(s).
     * @return self
     */
    public function addValue(string|array $value): self
    {
        if (is_array($value)) {
            foreach ($value as $val) {
                $this->value[] = self::sanitizeValue($val);
            }
        } else {
            $this->value[] = self::sanitizeValue($value);
        }

        return $this;
    }

    /**
     * Remove all values or one value from the attribute.
     *
     * @param string|null $value if set, remove only this value
     * @return self
     */
    public function removeValue(?string $value = null): self
    {
        if (is_null($value)) {
            $this->value = [];
        } elseif (($key = array_search($value, $this->value)) !== false) {
            unset($this->value[$key]);
        }

        return $this;
    }

    /**
     * Set the separator used to concatenate values.
     *
     * @param string $separator  Separator string.
     * @return self
     */
    public function setSeparator(string $separator): self
    {
        $this->separator = $separator;

        return $this;
    }

    /**
     * Get the separator used to concatenate values.
     *
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * Sanitize value against CSS attacks (Cross-Site Scripting)
     *
     * @param string|array $value
     *
     * @return string|array
     */
    public static function sanitizeValue(string|array $value): string|array
    {
        if (is_array($value)) {
            $value = self::sanitizeAllValues($value);
        } else {
            $value = \strip_tags($value);
            $value = htmlspecialchars($value, ENT_QUOTES);
        }

        return $value;
    }

    /**
     * Sanitize all values from array
     *
     * @param array $value
     *
     * @return array
     */
    public static function sanitizeAllValues(array $value): array
    {
        foreach ($value as $key => $val) {
            $value[$key] = self::sanitizeValue($val);
        }

        return $value;
    }

    /**
     * Build the attribute string.
     *
     * @return string
     */
    public function build(): string
    {
        $uniqueValues = \array_unique($this->getValue());
        $attrValuesHtml = implode($this->separator, $uniqueValues);

        return sprintf('%s="%s"', $this->getName(), $attrValuesHtml);
    }
}
