<?php

namespace AngleWeb\HtmlBuilder;

/**
 * Create ahref link tag
 */
class SvgSymbolBuilder extends TagBuilder
{
    private string $src;
    private int $width = 32;
    private int $height = 32;
    private string $ariaLabel;
    private bool $ariaHidden = false;
    private ?string $iconName = null;

    public function __construct(string $url, string $iconName = null)
    {
        $this->setTag('svg');
        $this->setSrc($url);

        if (isset($iconName)) {
            $this->setIconName($iconName);
        }
    }

    /**
     * Get the value of src
     */
    public function getSrc(): string
    {
        return $this->src;
    }

    /**
     * Set the value of src
     */
    public function setSrc(string $src): self
    {
        $this->src = $src;

        return $this;
    }

    /**
     * Get the value of width
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * Set the value of width
     */
    public function setWidth(int $width): self
    {
        if ($width > 0) {
            $this->width = $width;
            $this->setAttribute('width', $width);
        }

        return $this;
    }

    /**
     * Get the value of height
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * Set the value of height
     */
    public function setHeight(int $height): self
    {
        if ($height > 0) {
            $this->height = $height;
            $this->setAttribute('height', $height);
        }

        return $this;
    }

    public function setSize(int $width, int $height): self
    {
        $this->setWidth($width);
        $this->setHeight($height);

        return $this;
    }

    /**
     * Get the value of ariaHidden
     */
    public function isAriaHidden(): bool
    {
        return $this->ariaHidden;
    }

    /**
     * Set the value of ariaHidden
     */
    public function setAriaHidden(bool $ariaHidden): self
    {
        $this->ariaHidden = $ariaHidden;

        if ($ariaHidden) {
            $this->addAttribute('aria-hidden', 'true');
        }

        return $this;
    }

    /**
     * Get the value of iconName
     */
    public function getIconName(): ?string
    {
        return $this->iconName;
    }

    /**
     * Set the value of iconName
     */
    public function setIconName(?string $iconName): self
    {
        $this->iconName = $iconName;

        return $this;
    }

    /**
     * Get the value of ariaLabel
     */
    public function getAriaLabel(): string
    {
        return $this->ariaLabel;
    }

    /**
     * Set the value of ariaLabel
     */
    public function setAriaLabel(string $ariaLabel): self
    {
        $this->ariaLabel = $ariaLabel;
        $this->addAttribute('aria-label', $ariaLabel);

        return $this;
    }

    public function build(): string
    {
        $use = new TagBuilder('use');

        if (!empty($this->getSrc())) {
            $href = $this->getSrc();

            if (!empty($this->getIconName())) {
                $href .= '#' . $this->getIconName();
            }

            $use->addAttribute('xlink:href', $href);
        }

        $this->addChild($use);

        return parent::build();
    }
}
